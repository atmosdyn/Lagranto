.. _contents:


Welcome to Lagranto's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   tutorial_lagranto
   lagranto

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

